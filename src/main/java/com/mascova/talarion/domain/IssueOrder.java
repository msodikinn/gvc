package com.mascova.talarion.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "issue_order")
public class IssueOrder extends AbstractAuditingEntity implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "issue_date")
  private ZonedDateTime issueDate;
  
  @Column(name = "issue_no")
  private String issueNo;
  
  @Column(name = "issue_name")
  private String issueName;
  
  @Column(name = "issue_status")
  private String issueStatus;
  
  @Column(name = "customer")
  private String customer;
  
  @Column(name = "notes")
  private String notes;
  
  @ManyToOne
  @JoinColumn(name = "order_id")
  private Order order;
  

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}


public String getCustomer() {
	return customer;
}

public void setCustomer(String customer) {
	this.customer = customer;
}

public String getNotes() {
	return notes;
}

public void setNotes(String notes) {
	this.notes = notes;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}

public ZonedDateTime getIssueDate() {
	return issueDate;
}

public void setIssueDate(ZonedDateTime issueDate) {
	this.issueDate = issueDate;
}

public String getIssueNo() {
	return issueNo;
}

public void setIssueNo(String issueNo) {
	this.issueNo = issueNo;
}

public String getIssueStatus() {
	return issueStatus;
}

public void setIssueStatus(String issueStatus) {
	this.issueStatus = issueStatus;
}

public Order getOrder() {
	return order;
}

public void setOrder(Order order) {
	this.order = order;
}

public String getIssueName() {
	return issueName;
}

public void setIssueName(String issueName) {
	this.issueName = issueName;
}



}
