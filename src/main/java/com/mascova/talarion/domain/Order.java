package com.mascova.talarion.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "m_order")
public class Order extends AbstractAuditingEntity implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(name = "order_date")
  private ZonedDateTime orderDate;
  
  @Column(name = "order_no")
  private String orderNo;
  
  @Column(name = "order_status")
  private Character orderStatus;
  
  @Column(name = "customer")
  private String customer;
  
  @Column(name = "notes")
  private String notes;
  
  private Character ispurchase;

public Long getId() {
	return id;
}

public void setId(Long id) {
	this.id = id;
}

public ZonedDateTime getOrderDate() {
	return orderDate;
}

public void setOrderDate(ZonedDateTime orderDate) {
	this.orderDate = orderDate;
}

public String getOrderNo() {
	return orderNo;
}

public void setOrderNo(String orderNo) {
	this.orderNo = orderNo;
}



public Character getOrderStatus() {
	return orderStatus;
}

public void setOrderStatus(Character orderStatus) {
	this.orderStatus = orderStatus;
}

public String getCustomer() {
	return customer;
}

public void setCustomer(String customer) {
	this.customer = customer;
}

public String getNotes() {
	return notes;
}

public void setNotes(String notes) {
	this.notes = notes;
}

public static long getSerialversionuid() {
	return serialVersionUID;
}

public Character getIspurchase() {
	return ispurchase;
}

public void setIspurchase(Character ispurchase) {
	this.ispurchase = ispurchase;
}
  
  
}
