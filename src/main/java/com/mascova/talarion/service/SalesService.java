package com.mascova.talarion.service;

import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.mascova.talarion.domain.SalesHead;
import com.mascova.talarion.domain.SalesItem;
import com.mascova.talarion.repository.SalesHeadRepository;
import com.mascova.talarion.repository.SalesItemRepository;

@Service
public class SalesService {
  
  private SalesHeadRepository salesHeadRepository;
  
  private SalesItemRepository salesItemRepository;

  public SalesService(SalesHeadRepository salesHeadRepository, SalesItemRepository salesItemRepository) {
	super();
	this.salesHeadRepository = salesHeadRepository;
	this.salesItemRepository = salesItemRepository;
}

public void createSales(SalesHead salesHead) {

    SalesHead bareSalesHead = new SalesHead();
    BeanUtils.copyProperties(salesHead, bareSalesHead, "salesItems");
    Set<SalesItem> transientSalesItems = salesHead.getSalesItems();

    bareSalesHead = salesHeadRepository.save(bareSalesHead);
    bareSalesHead.setSalesItems(salesHead.getSalesItems());

  }

}
