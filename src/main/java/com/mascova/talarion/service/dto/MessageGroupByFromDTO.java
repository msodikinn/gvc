package com.mascova.talarion.service.dto;

import java.io.Serializable;

import com.mascova.talarion.domain.User;

public class MessageGroupByFromDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long count;
	private String from;
	
	public Long getCount() {
		return count;
	}
	public void setCount(Long count) {
		this.count = count;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}

	
	

}
