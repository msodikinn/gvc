package com.mascova.talarion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mascova.talarion.domain.ProductStock;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface ProductStockRepository extends JpaRepository<ProductStock, Long>,
    JpaSpecificationExecutor<ProductStock> {


}
