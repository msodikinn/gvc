package com.mascova.talarion.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.mascova.talarion.domain.IssueOrder;
import com.mascova.talarion.domain.Order;

public class IssueOrderSpecificationBuilder {

  private final List<SearchCriteria> params;

  public IssueOrderSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public IssueOrderSpecificationBuilder with(String key, String operation, Object value) {
    params.add(new SearchCriteria(key, operation, value));
    return this;
  }

  public Specification<IssueOrder> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<IssueOrder>> specs = new ArrayList<Specification<IssueOrder>>();
    for (SearchCriteria param : params) {
      specs.add(new IssueOrderSpecification(param));
    }

    Specification<IssueOrder> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
    }
    return result;
  }

}
