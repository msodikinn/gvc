package com.mascova.talarion.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.mascova.talarion.domain.Order;
import com.mascova.talarion.domain.OrderDetil;

public class OrderDetilSpecificationBuilder {

  private final List<SearchCriteria> params;

  public OrderDetilSpecificationBuilder() {
    params = new ArrayList<SearchCriteria>();
  }

  public OrderDetilSpecificationBuilder with(String key, String operation, Object value) {
    params.add(new SearchCriteria(key, operation, value));
    return this;
  }

  public Specification<OrderDetil> build() {
    if (params.size() == 0) {
      return null;
    }

    List<Specification<OrderDetil>> specs = new ArrayList<Specification<OrderDetil>>();
    for (SearchCriteria param : params) {
      specs.add(new OrderDetilSpecification(param));
    }

    Specification<OrderDetil> result = specs.get(0);
    for (int i = 1; i < specs.size(); i++) {
      result = Specifications.where(result).and(specs.get(i));
    }
    return result;
  }

}
