package com.mascova.talarion.repository.specification;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.mascova.talarion.domain.Message;

public class MessageSpecificationBuilder {

	private final List<SearchCriteria> params;

	public MessageSpecificationBuilder() {
		params = new ArrayList<SearchCriteria>();
	}

	public MessageSpecificationBuilder with(String key, String operation, Object value) {
		params.add(new SearchCriteria(key, operation, value));
		return this;
	}

	public Specification<Message> build(String from, String to) {

		Specification<Message> result = null;
		if (params.size() != 0) {
			List<Specification<Message>> specs = new ArrayList<Specification<Message>>();
			for (SearchCriteria param : params) {
				specs.add(new MessageSpecification(param));
			}

			result = specs.get(0);
			for (int i = 1; i < specs.size(); i++) {
				result = Specifications.where(result).and(specs.get(i));
			}
		}

		// BEGIN Additional for Message Conversation between two people
		MessageSpecification from1 = new MessageSpecification(new SearchCriteria("from.login", ":", from));
		MessageSpecification to1 = new MessageSpecification(new SearchCriteria("to.login", ":", to));

		MessageSpecification from2 = new MessageSpecification(new SearchCriteria("from.login", ":", to));
		MessageSpecification to2 = new MessageSpecification(new SearchCriteria("to.login", ":", from));

		Specification<Message> conv = Specifications.where(Specifications.where(from1).and(to1))
				.or(Specifications.where(from2).and(to2));
		if (result != null) {
			result = Specifications.where(result).and(conv);
		} else {
			return conv;
		}			
		
		// END Additional for Message Conversation between two people

		return result;
	}

}
