package com.mascova.talarion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mascova.talarion.domain.Notification;

/**
 * Spring Data JPA repository for the Category entity.
 */
public interface NotificationRepository extends JpaRepository<Notification, Long>,
    JpaSpecificationExecutor<Notification> {

}
