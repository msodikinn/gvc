package com.mascova.talarion.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.mascova.talarion.domain.Message;
import com.mascova.talarion.domain.User;

/**
 * Spring Data JPA repository for the Category entity.
 */
public interface MessageRepository extends JpaRepository<Message, Long>, JpaSpecificationExecutor<Message> {

	@Query(value = "SELECT COUNT(m.from.login) as count, u as user FROM Message m JOIN m.from u WHERE m.read = :read AND m.to.login = :login GROUP BY u ORDER BY m.from.login")
	public List<Object[]> findAllGroupByFrom(@Param("login") String login, @Param("read") Boolean read, Pageable pageable);

	public List<Message> findTop10ByOrderByCreatedDesc();

	@Query(value = "SELECT COUNT(m) FROM Message m WHERE m.read = false AND m.to = :to ")
	public long countByTo(@Param("to") User to);

	@Modifying(clearAutomatically = true)
	@Transactional
	@Query(value = "UPDATE Message m SET m.read = TRUE WHERE m.read = FALSE AND m.from = :from AND m.to = :to")
	public void setRead(@Param("from") User from, @Param("to") User to);

}
