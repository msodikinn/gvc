package com.mascova.talarion.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.mascova.talarion.domain.IssueOrder;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface IssueOrderRepository extends JpaRepository<IssueOrder, Long>,
    JpaSpecificationExecutor<IssueOrder> {


}
