package com.mascova.talarion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mascova.talarion.domain.Applicant;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

}