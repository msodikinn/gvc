package com.mascova.talarion.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.mascova.talarion.domain.Order;
import com.mascova.talarion.domain.OrderDetil;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface OrderDetilRepository extends JpaRepository<OrderDetil, Long>,
    JpaSpecificationExecutor<OrderDetil> {

	List<OrderDetil> findByOrder(Order order);
}
