package com.mascova.talarion.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mascova.talarion.domain.DownloadSession;

/**
 * Spring Data JPA repository for the DownloadSession entity.
 */
@SuppressWarnings("unused")
public interface DownloadSessionRepository extends JpaRepository<DownloadSession, Long> {

	DownloadSession findByToken(String token);

}
