package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.Order;
import com.mascova.talarion.repository.OrderRepository;
import com.mascova.talarion.repository.specification.OrderSpecificationBuilder;
import com.mascova.talarion.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Order.
 */
@RestController
@RequestMapping("/api")
public class OrderResource {

  private final Logger log = LoggerFactory.getLogger(OrderResource.class);
  
  private OrderRepository orderRepository;

  public OrderResource(OrderRepository orderRepository) {
	super();
	this.orderRepository = orderRepository;
}

/**
   * POST /order -> Create a new order.
   */
  @RequestMapping(value = "/order", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Order> create(@RequestBody Order order) throws URISyntaxException {
    log.debug("REST request to save Order : {}", order);
    if (order.getId() != null) {
      return ResponseEntity.badRequest()
          .header("Failure", "A new order cannot already have an ID").build();
    }
    Order createOrder = orderRepository.save(order);
    return Optional.ofNullable(createOrder)
            .map(success -> new ResponseEntity<>(createOrder, HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
//    return ResponseEntity.created(new URI("/api/order/" + order.getId())).build();
  }

  /**
   * PUT /order -> Updates an existing order.
   */
  @RequestMapping(value = "/order", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> update(@RequestBody Order order) throws URISyntaxException {
    log.debug("REST request to update Order : {}", order);
//    if (order.getId() == null) {
//      return create(order);
//    }
    orderRepository.save(order);
    return ResponseEntity.ok().build();
  }

  /**
   * GET /order -> get all the orders.
   */
  @RequestMapping(value = "/order", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<Order>> getAll(
      @RequestParam(value = "page", required = false) Integer offset,
      @RequestParam(value = "size", required = false) Integer size,
      @RequestParam(value = "orderNo", required = false) String orderNo,
      @RequestParam(value = "orderStatus", required = false) String orderStatus,
      @RequestParam(value = "customer", required = false) String customer,
      @RequestParam(value = "ispurchase", required = false) Character ispurchase) throws URISyntaxException {

    OrderSpecificationBuilder builder = new OrderSpecificationBuilder();

    if (StringUtils.isNotBlank(orderNo)) {
      builder.with("orderNo", ":", orderNo);
    }
    if (StringUtils.isNotBlank(orderStatus)) {
        builder.with("orderStatus", ":", orderStatus);
    }
    if (StringUtils.isNotBlank(customer)) {
        builder.with("customer", ":", customer);
    }
    if (ispurchase != null) {
        builder.with("ispurchase", ":", ispurchase);
    }
    

    Page<Order> page = orderRepository.findAll(builder.build(),
        PaginationUtil.generatePageRequest(offset, size));

    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/order");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET /order/:id -> get the "id" order.
   */
  @RequestMapping(value = "/order/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Order> get(@PathVariable Long id) {
    log.debug("REST request to get Order : {}", id);
    return Optional.ofNullable(orderRepository.findOne(id))
        .map(order -> new ResponseEntity<>(order, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * DELETE /order/:id -> delete the "id" order.
   */
  @RequestMapping(value = "/order/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public void delete(@PathVariable Long id) {
    log.debug("REST request to delete Order : {}", id);
    orderRepository.delete(id);
  }
}
