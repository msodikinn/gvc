package com.mascova.talarion.web.rest;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mascova.talarion.domain.DownloadSession;
import com.mascova.talarion.repository.DownloadSessionRepository;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

/**
 * REST controller for managing Download.
 */
@RestController
@RequestMapping("/download")
public class DownloadResource {

	private final Logger log = LoggerFactory.getLogger(DownloadResource.class);

	private DataSource dataSource;

	private DownloadSessionRepository downloadSessionRepository;

	public DownloadResource(DataSource dataSource, DownloadSessionRepository downloadSessionRepository) {
		super();
		this.dataSource = dataSource;
		this.downloadSessionRepository = downloadSessionRepository;
	}

	/**
	 * GET /authors/:id : get the "id" author.
	 *
	 * @param id
	 *            the id of the author to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the author,
	 *         or with status 404 (Not Found)
	 */
	/**
	 * GET /category/:id -> get the "id" category.
	 * 
	 * @throws IOException
	 */
	@RequestMapping(value = "/report/xlsx/product/{token}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public void get(HttpServletRequest request, HttpServletResponse response, @PathVariable String token)
			throws IOException {
		log.debug("REST request to get Report : {}");

		DownloadSession downloadSession = downloadSessionRepository.findByToken(token);

		if (downloadSession == null) {
			response.getWriter().println("Invalid Download Token");
		} else {

			downloadSessionRepository.delete(downloadSession);

			File reportFile = new File(System.getProperty("user.dir") + java.io.File.separator + "src"
					+ java.io.File.separator + "main" + java.io.File.separator + "resources" + java.io.File.separator
					+ "report" + java.io.File.separator + "jasper" + java.io.File.separator + "xlsx-product.jasper");

			JasperReport jasperReport = null;
			JasperPrint jasperPrint = null;
			byte[] bytes = null;
			ByteArrayOutputStream os = null;

			Map<String, Object> parameters = new HashMap<String, Object>();
			// parameters.put("TIMESHEET_ID", timesheet.getId());
			parameters.put("BANNER_PATH",
					System.getProperty("user.dir") + java.io.File.separator + "src" + java.io.File.separator + "main"
							+ java.io.File.separator + "resources" + java.io.File.separator + "report"
							+ java.io.File.separator + "jasper" + java.io.File.separator + "leaf_banner_red.png");

			try {
				jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource.getConnection());
				// bytes = JasperRunManager.runReportToPdf(jasperReport,
				// parameters,
				// dataSource.getConnection());

				JRXlsxExporter exporter = new JRXlsxExporter();
				os = new ByteArrayOutputStream();
				exporter.setExporterInput(SimpleExporterInput.getInstance(Collections.singletonList(jasperPrint)));
				exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));
				SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
				// configuration.set
				//
				exporter.setConfiguration(configuration);

				// exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT,
				// jasperPrint);
				// exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME,
				// outputFileName);

				exporter.exportReport();
			} catch (JRException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			response.resetBuffer();
			response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setHeader("Content-Disposition", "attachment;filename=products.xlsx");
			response.setHeader("x-filename", "products.xlsx");
			response.setHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
			response.setContentLength(os.toByteArray().length);
			ServletOutputStream ouputStream;
			try {
				ouputStream = response.getOutputStream();
				ouputStream.write(os.toByteArray(), 0, os.toByteArray().length);
				ouputStream.flush();
				ouputStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
