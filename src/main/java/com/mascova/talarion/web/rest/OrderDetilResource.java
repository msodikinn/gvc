package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.Order;
import com.mascova.talarion.domain.OrderDetil;
import com.mascova.talarion.repository.OrderDetilRepository;
import com.mascova.talarion.repository.OrderRepository;
import com.mascova.talarion.repository.specification.OrderDetilSpecificationBuilder;
import com.mascova.talarion.repository.specification.OrderSpecificationBuilder;
import com.mascova.talarion.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Order.
 */
@RestController
@RequestMapping("/api")
public class OrderDetilResource {

  private final Logger log = LoggerFactory.getLogger(OrderDetilResource.class);
  
  private OrderDetilRepository orderDetilRepository;
  private OrderRepository orderRepository;

  public OrderDetilResource(OrderDetilRepository orderDetilRepository,OrderRepository orderRepository) {
	super();
	this.orderDetilRepository = orderDetilRepository;
	this.orderRepository = orderRepository;
}

/**
   * POST /order -> Create a new order.
   */
  @RequestMapping(value = "/orderDetil", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> create(@RequestBody OrderDetil orderDetil) throws URISyntaxException {
    log.debug("REST request to save OrderDetil : {}", orderDetil);
    if (orderDetil.getId() != null) {
      return ResponseEntity.badRequest()
          .header("Failure", "A new order cannot already have an ID").build();
    }
    orderDetilRepository.save(orderDetil);
    return ResponseEntity.created(new URI("/api/orderDetil/" + orderDetil.getId())).build();
  }

  /**
   * PUT /order -> Updates an existing order.
   */
  @RequestMapping(value = "/orderDetil", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> update(@RequestBody OrderDetil orderDetil) throws URISyntaxException {
    log.debug("REST request to update OrderDetil : {}", orderDetil);
    if (orderDetil.getId() == null) {
      return create(orderDetil);
    }
    orderDetilRepository.save(orderDetil);
    return ResponseEntity.ok().build();
  }

  /**
   * GET /order -> get all the orders.
   */
  @RequestMapping(value = "/orderDetil", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<OrderDetil>> getAll(
      @RequestParam(value = "page", required = false) Integer offset,
      @RequestParam(value = "size", required = false) Integer size,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "name2", required = false) String name2) throws URISyntaxException {

    OrderDetilSpecificationBuilder builder = new OrderDetilSpecificationBuilder();

    if (StringUtils.isNotBlank(name)) {
      builder.with("name", ":", name);
    }

    Page<OrderDetil> page = orderDetilRepository.findAll(builder.build(),
        PaginationUtil.generatePageRequest(offset, size));

    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/orderDetil");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET /order/:id -> get the "id" order.
   */
  @RequestMapping(value = "/orderDetil/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<OrderDetil> get(@PathVariable Long id) {
    log.debug("REST request to get Order : {}", id);
    return Optional.ofNullable(orderDetilRepository.findOne(id))
        .map(order -> new ResponseEntity<>(order, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * DELETE /order/:id -> delete the "id" order.
   */
  @RequestMapping(value = "/orderDetil/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public void delete(@PathVariable Long id) {
    log.debug("REST request to delete OrderDetil : {}", id);
    orderDetilRepository.delete(id);
  }
  
  /**
   * GET /category -> get all the categorys.
   */
  @RequestMapping(value = "/orderDetil/order/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<OrderDetil>> getDebiturCif(@PathVariable Long id) throws URISyntaxException {
	  	
	  	Order orderdb = orderRepository.findOne(id);
		List<OrderDetil> result = orderDetilRepository.findByOrder(orderdb);
		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<>(result, headers, HttpStatus.OK);
  }
}
