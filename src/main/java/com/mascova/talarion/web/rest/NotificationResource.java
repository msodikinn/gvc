package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.Notification;
import com.mascova.talarion.repository.NotificationRepository;
import com.mascova.talarion.web.rest.util.PaginationUtil;

/**
 * REST controller for managing Notification.
 */
@RestController
@RequestMapping("/api")
public class NotificationResource {

	private final Logger log = LoggerFactory.getLogger(NotificationResource.class);
	
	private NotificationRepository notificationRepository;

	public NotificationResource(NotificationRepository notificationRepository) {
		super();
		this.notificationRepository = notificationRepository;
	}

	/**
	 * POST /notification -> Create a new notification.
	 */
	@RequestMapping(value = "/notification", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> create(@RequestBody Notification notification) throws URISyntaxException {
		log.debug("REST request to save Notification : {}", notification);
		if (notification.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new notification cannot already have an ID")
					.build();
		}
		notificationRepository.save(notification);
		return ResponseEntity.created(new URI("/api/notification/" + notification.getId())).build();
	}

	/**
	 * PUT /notification -> Updates an existing notification.
	 */
	@RequestMapping(value = "/notification", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> update(@RequestBody Notification notification) throws URISyntaxException {
		log.debug("REST request to update Notification : {}", notification);
		if (notification.getId() == null) {
			return create(notification);
		}
		notificationRepository.save(notification);
		return ResponseEntity.ok().build();
	}

	/**
	 * GET /notification -> get all the notifications.
	 */
	@RequestMapping(value = "/notification", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<Notification>> getAll(@RequestParam(value = "page", required = false) Integer offset,
			@RequestParam(value = "size", required = false) Integer size) throws URISyntaxException {

		Page<Notification> page = notificationRepository.findAll(PaginationUtil.generatePageRequest(offset, size));

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/notification");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /notification/:id -> get the "id" notification.
	 */
	@RequestMapping(value = "/notification/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Notification> get(@PathVariable Long id) {
		log.debug("REST request to get Notification : {}", id);
		return Optional.ofNullable(notificationRepository.findOne(id))
				.map(notification -> new ResponseEntity<>(notification, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /notification/:id -> delete the "id" notification.
	 */
	@RequestMapping(value = "/notification/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public void delete(@PathVariable Long id) {
		log.debug("REST request to delete Notification : {}", id);
		notificationRepository.delete(id);
	}
}
