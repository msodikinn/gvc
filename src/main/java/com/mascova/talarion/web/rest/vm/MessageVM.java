package com.mascova.talarion.web.rest.vm;

import java.util.List;

public class MessageVM {

	private Long countUnread;

	List<Object[]> groupByFrom;

	public Long getCountUnread() {
		return countUnread;
	}

	public void setCountUnread(Long countUnread) {
		this.countUnread = countUnread;
	}

	public List<Object[]> getGroupByFrom() {
		return groupByFrom;
	}

	public void setGroupByFrom(List<Object[]> groupByFrom) {
		this.groupByFrom = groupByFrom;
	}

}
