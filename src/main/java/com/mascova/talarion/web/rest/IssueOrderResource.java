package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.IssueOrder;
import com.mascova.talarion.repository.IssueOrderRepository;
import com.mascova.talarion.repository.specification.IssueOrderSpecificationBuilder;
import com.mascova.talarion.web.rest.util.PaginationUtil;

/**
 * REST controller for managing IssueOrder.
 */
@RestController
@RequestMapping("/api")
public class IssueOrderResource {

  private final Logger log = LoggerFactory.getLogger(IssueOrderResource.class);
  
  private IssueOrderRepository issueorderRepository;

  public IssueOrderResource(IssueOrderRepository issueorderRepository) {
	super();
	this.issueorderRepository = issueorderRepository;
}

/**
   * POST /issueorder -> Create a new issueorder.
   */
  @RequestMapping(value = "/issueorder", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> create(@RequestBody IssueOrder issueorder) throws URISyntaxException {
    log.debug("REST request to save IssueOrder : {}", issueorder);
    if (issueorder.getId() != null) {
      return ResponseEntity.badRequest()
          .header("Failure", "A new issueorder cannot already have an ID").build();
    }
    issueorderRepository.save(issueorder);
    return ResponseEntity.created(new URI("/api/issueorder/" + issueorder.getId())).build();
  }

  /**
   * PUT /issueorder -> Updates an existing issueorder.
   */
  @RequestMapping(value = "/issueorder", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<Void> update(@RequestBody IssueOrder issueorder) throws URISyntaxException {
    log.debug("REST request to update IssueOrder : {}", issueorder);
    if (issueorder.getId() == null) {
      return create(issueorder);
    }
    issueorderRepository.save(issueorder);
    return ResponseEntity.ok().build();
  }

  /**
   * GET /issueorder -> get all the issueorders.
   */
  @RequestMapping(value = "/issueorder", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<List<IssueOrder>> getAll(
      @RequestParam(value = "page", required = false) Integer offset,
      @RequestParam(value = "size", required = false) Integer size,
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "name2", required = false) String name2) throws URISyntaxException {

    IssueOrderSpecificationBuilder builder = new IssueOrderSpecificationBuilder();

    if (StringUtils.isNotBlank(name)) {
      builder.with("name", ":", name);
    }

    Page<IssueOrder> page = issueorderRepository.findAll(builder.build(),
        PaginationUtil.generatePageRequest(offset, size));

    HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/issueorder");
    return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
  }

  /**
   * GET /issueorder/:id -> get the "id" issueorder.
   */
  @RequestMapping(value = "/issueorder/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public ResponseEntity<IssueOrder> get(@PathVariable Long id) {
    log.debug("REST request to get IssueOrder : {}", id);
    return Optional.ofNullable(issueorderRepository.findOne(id))
        .map(issueorder -> new ResponseEntity<>(issueorder, HttpStatus.OK))
        .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
  }

  /**
   * DELETE /issueorder/:id -> delete the "id" issueorder.
   */
  @RequestMapping(value = "/issueorder/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
  @Timed
  public void delete(@PathVariable Long id) {
    log.debug("REST request to delete IssueOrder : {}", id);
    issueorderRepository.delete(id);
  }
}
