/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mascova.talarion.web.rest.vm;
