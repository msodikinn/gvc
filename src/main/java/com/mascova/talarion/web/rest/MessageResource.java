package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.Message;
import com.mascova.talarion.domain.User;
import com.mascova.talarion.repository.MessageRepository;
import com.mascova.talarion.repository.UserRepository;
import com.mascova.talarion.repository.specification.MessageSpecificationBuilder;
import com.mascova.talarion.security.SecurityUtils;
import com.mascova.talarion.web.rest.util.PaginationUtil;
import com.mascova.talarion.web.rest.vm.MessageVM;

/**
 * REST controller for managing Message.
 */
@RestController
@RequestMapping("/api")
public class MessageResource {

	private final Logger log = LoggerFactory.getLogger(MessageResource.class);

	private MessageRepository messageRepository;

	private UserRepository userRepository;

	public MessageResource(MessageRepository messageRepository, UserRepository userRepository) {
		super();
		this.messageRepository = messageRepository;
		this.userRepository = userRepository;
	}

	/**
	 * POST /message -> Create a new message.
	 */
	@RequestMapping(value = "/message", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> create(@RequestBody Message message) throws URISyntaxException {
		log.debug("REST request to save Message : {}", message);
		if (message.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new message cannot already have an ID").build();
		}

		User from = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
		message.setFrom(from);

		messageRepository.save(message);
		return ResponseEntity.created(new URI("/api/message/" + message.getId())).build();
	}

	/**
	 * PUT /message -> Updates an existing message.
	 */
	@RequestMapping(value = "/message", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> update(@RequestBody Message message) throws URISyntaxException {
		log.debug("REST request to update Message : {}", message);
		if (message.getId() == null) {
			return create(message);
		}
		messageRepository.save(message);
		return ResponseEntity.ok().build();
	}

	/**
	 * GET /message -> get all the messages.
	 */
	@RequestMapping(value = "/message", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<Message>> getAll(@RequestParam(value = "from", required = false) String from,
			@RequestParam(value = "page", required = false) Integer offset,
			@RequestParam(value = "size", required = false) Integer size) throws URISyntaxException {

		MessageSpecificationBuilder builder = new MessageSpecificationBuilder();

		Long totalMessages = messageRepository.count(builder.build(SecurityUtils.getCurrentUserLogin(), from));
		Double lastPage = Math.ceil(totalMessages.doubleValue() / size.doubleValue());

		Sort sortObj = new Sort(Sort.Direction.ASC, "created");
		Page<Message> page = messageRepository.findAll(builder.build(SecurityUtils.getCurrentUserLogin(), from),
				PaginationUtil.generatePageRequest(lastPage.intValue(), size, sortObj));

		// List<Message> list =
		// messageRepository.findTop10ByOrderByCreatedDesc();

		User userFrom = userRepository.findOneByLogin(from).get();
		User userTo = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
		messageRepository.setRead(userFrom, userTo);

		HttpHeaders headers = new HttpHeaders();
		// HttpHeaders headers =
		// PaginationUtil.generatePaginationHttpHeaders(list, "/api/message");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /message/:id -> get the "id" message.
	 */
	@RequestMapping(value = "/message/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Message> get(@PathVariable Long id) {
		log.debug("REST request to get Message : {}", id);
		return Optional.ofNullable(messageRepository.findOne(id))
				.map(message -> new ResponseEntity<>(message, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /message/:id -> delete the "id" message.
	 */
	@RequestMapping(value = "/message/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public void delete(@PathVariable Long id) {
		log.debug("REST request to delete Message : {}", id);
		messageRepository.delete(id);
	}

	/**
	 * GET /message -> get all the messages.
	 */
	@RequestMapping(value = "/message/groupByFrom", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<List<Object[]>> groupByFrom(
			@RequestParam(value = "read", required = false, defaultValue = "false") String read,
			@RequestParam(value = "page", required = false) Integer offset,
			@RequestParam(value = "size", required = false) Integer size) throws URISyntaxException {

		List<Object[]> map = messageRepository.findAllGroupByFrom(SecurityUtils.getCurrentUserLogin(),
				new Boolean(read), PaginationUtil.generatePageRequest(0, 5));

		HttpHeaders headers = new HttpHeaders();
		return new ResponseEntity<>(map, headers, HttpStatus.OK);
	}

	/**
	 * GET /message/:id -> get the "id" message.
	 */
	@RequestMapping(value = "/message/self/countUnread", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<MessageVM> selfCountUnread() {
		HttpHeaders headers = new HttpHeaders();

		User userTo = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).get();
		Long countUnread = messageRepository.countByTo(userTo);

		List<Object[]> groupByFrom = messageRepository.findAllGroupByFrom(SecurityUtils.getCurrentUserLogin(), false,
				PaginationUtil.generatePageRequest(0, 5));

		MessageVM messageVM = new MessageVM();
		messageVM.setCountUnread(countUnread);
		messageVM.setGroupByFrom(groupByFrom);

		return new ResponseEntity<>(messageVM, headers, HttpStatus.OK);
	}
}
