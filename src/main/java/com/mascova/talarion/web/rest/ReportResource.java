package com.mascova.talarion.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import javax.sql.DataSource;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.mascova.talarion.domain.DownloadSession;
import com.mascova.talarion.repository.DownloadSessionRepository;

/**
 * REST controller for managing Category.
 */
@RestController
@RequestMapping("/api")
public class ReportResource {

	private final Logger log = LoggerFactory.getLogger(ReportResource.class);

	private DataSource dataSource;

	private DownloadSessionRepository downloadSessionRepository;

	public ReportResource(DataSource dataSource, DownloadSessionRepository downloadSessionRepository) {
		super();
		this.dataSource = dataSource;
		this.downloadSessionRepository = downloadSessionRepository;
	}

	// /**
	// * GET /category/:id -> get the "id" category.
	// */
	// @RequestMapping(value = "/report/xlsx/product", method =
	// RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	// public void get(HttpServletRequest request, HttpServletResponse response)
	// {
	// log.debug("REST request to get Report : {}");
	//
	// // Validate here
	//
	// File reportFile = new File(System.getProperty("user.dir") +
	// java.io.File.separator + "src"
	// + java.io.File.separator + "main" + java.io.File.separator + "resources"
	// + java.io.File.separator
	// + "report" + java.io.File.separator + "jasper" + java.io.File.separator +
	// "xlsx-product.jasper");
	//
	// JasperReport jasperReport = null;
	// JasperPrint jasperPrint = null;
	// byte[] bytes = null;
	// ByteArrayOutputStream os = null;
	//
	// Map<String, Object> parameters = new HashMap<String, Object>();
	// // parameters.put("TIMESHEET_ID", timesheet.getId());
	// parameters.put("BANNER_PATH",
	// System.getProperty("user.dir") + java.io.File.separator + "src" +
	// java.io.File.separator + "main"
	// + java.io.File.separator + "resources" + java.io.File.separator +
	// "report"
	// + java.io.File.separator + "jasper" + java.io.File.separator +
	// "leaf_banner_red.png");
	//
	// try {
	// jasperReport = (JasperReport)
	// JRLoader.loadObjectFromFile(reportFile.getPath());
	// jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
	// dataSource.getConnection());
	// // bytes = JasperRunManager.runReportToPdf(jasperReport, parameters,
	// // dataSource.getConnection());
	//
	// JRXlsxExporter exporter = new JRXlsxExporter();
	// os = new ByteArrayOutputStream();
	// exporter.setExporterInput(SimpleExporterInput.getInstance(Collections.singletonList(jasperPrint)));
	// exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));
	// SimpleXlsxReportConfiguration configuration = new
	// SimpleXlsxReportConfiguration();
	// // configuration.set
	// //
	// exporter.setConfiguration(configuration);
	//
	// // exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT,
	// // jasperPrint);
	// // exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME,
	// // outputFileName);
	//
	// exporter.exportReport();
	// } catch (JRException | SQLException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// response.resetBuffer();
	// response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	// response.setHeader("Content-Disposition",
	// "attachment;filename=products.xlsx");
	// response.setHeader("x-filename", "products.xlsx");
	// response.setHeader("Content-Type",
	// "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	// response.setContentLength(os.toByteArray().length);
	// ServletOutputStream ouputStream;
	// try {
	// ouputStream = response.getOutputStream();
	// ouputStream.write(os.toByteArray(), 0, os.toByteArray().length);
	// ouputStream.flush();
	// ouputStream.close();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }

	/**
	 * GET /product/:id -> get the "id" product.
	 * 
	 * @throws URISyntaxException
	 */
	@RequestMapping(value = "report/xlsx/product", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> get() throws URISyntaxException {
		log.debug("REST request to get Download Session for Report");

		DownloadSession downloadSession = new DownloadSession();
		String token = RandomStringUtils.random(32, true, true);
		downloadSession.setToken(token);
		downloadSession.setFileName("report/xlsx/product");
		downloadSessionRepository.save(downloadSession);

		HttpHeaders headers = new HttpHeaders();
		headers.add("downloadUrl", "/download/report/xlsx/product/" + downloadSession.getToken());

		return new ResponseEntity<>(headers, HttpStatus.OK);

		// return ResponseEntity.created(new
		// URI("/download/report/xlsx/product/" +
		// downloadSession.getToken())).build();
	}

}
