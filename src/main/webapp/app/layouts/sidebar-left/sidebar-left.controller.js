(function() {
    'use strict';

    angular
        .module('talarionApp')
        .controller('SidebarLeftController', SidebarLeftController);

    SidebarLeftController.$inject = ['$location', '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', '$rootScope', '$http', 'Message'];

    function SidebarLeftController($location, $state, Auth, Principal, ProfileService, LoginService, $rootScope, $http, Message) {
        var vm = this;

        vm.navCollapsed = true;
        vm.isAuthenticated = Principal.isAuthenticated;

        ProfileService.getProfileInfo().then(function(response) {
            vm.inProduction = response.inProduction;
            vm.swaggerEnabled = response.swaggerEnabled;
        });

        vm.login = login;
        vm.logout = logout;
        vm.downloadFile = downloadFile;
        vm.downloadFile2 = downloadFile2;
        vm.$state = $state;

        // Define $rootScope.account if html is refreshed
        Principal.identity().then(function(account) {
            $rootScope.account = account;
            console.log("account is: " + JSON.stringify(account))

            // Get Unread Message count
            Message.selfCountUnread(function(result) {

                // console.log('$rootScope.messages.unread" ' + result);
                $rootScope.messageVM = result;
            });
        });

        function login() {
            LoginService.open();
        }

        function logout() {
            Auth.logout();
            $state.go('home');
        }

        function downloadFile(url) {

            console.log('test');

            $http({
                method: 'GET',
                url: url,
                responseType: 'arraybuffer'
            }).success(function(data, status, headers) {
                headers = headers();

                var filename = headers['x-filename'];
                var contentType = headers['Content-Type'];
                var linkElement = document.createElement('a');

                try {

                    var file = new Blob([data], {
                        type: contentType
                    });

                    var url = window.URL.createObjectURL(file);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });

                    linkElement.dispatchEvent(clickEvent);

                } catch (ex) {
                    console.log(ex);
                }

            }).error(function(data) {
                console.log(data);
            });

        }

        function downloadFile2(url) {

          console.log('ok');

            $http({
                method: 'GET',
                url: url
            }).then(function successCallback(response) {
                // this callback will be called asynchronously
                // when the response is available

                console.log(response.headers('downloadUrl'));

                var downloadUrl = response.headers('downloadUrl');

                downloadFile(downloadUrl);

            }, function errorCallback(response) {

                console.log(response);
                // called asynchronously if an error occurs
                // or server returns response with an error status.
            });

        }
    }
})();
