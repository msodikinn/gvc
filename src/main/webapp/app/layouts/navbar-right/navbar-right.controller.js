(function() {
    'use strict';

    angular
        .module('talarionApp')
        .controller('NavbarRightController', NavbarRightController);

    NavbarRightController.$inject = ['$location', '$state', 'Auth', 'Principal', 'ProfileService', 'LoginService', '$rootScope', '$http', 'Message'];

    function NavbarRightController($location, $state, Auth, Principal, ProfileService, LoginService, $rootScope, $http, Message) {
        var vm = this;

        vm.isAuthenticated = Principal.isAuthenticated;



    }
})();
