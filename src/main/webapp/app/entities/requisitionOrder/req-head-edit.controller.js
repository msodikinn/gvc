'use strict';

/**
 * @ngdoc function
 * @name yoAngularApp.controller:AboutCtrl
 * @description # AboutCtrl Controller of the yoAngularApp
 */
angular.module('talarionApp').controller('ReqHeadEditController',
    function($scope, $state, $stateParams, $http, Order, growl,OrderDetil,Product) {

        $scope.submitted = false;

        $scope.ro = {};
        $scope.rodetil = {};

        $scope.productList = Product.query({page: 1,
                size: 100});
        $scope.dateOptions = {
            formatYear: 'yyyy',
            //minDate: new Date(),
            startingDay: 1
        };

        $scope.popupStartDate = {
            opened: false
        };

        $scope.format = 'yyyy-MM-dd';
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.openStartDate = function() {
            $scope.popupStartDate.opened = true;
        };

        $scope.save = function() {
            // console.log("masuk ")
            // $scope.submitted = true;

            // if ($scope.editForm.$valid) {
            //     console.log("sini ")
                Order.update($scope.ro, function() {

                    growl.info("Request Order successfully edited ", {});

                    $state.go('req-head')
                });

            //}

        };
        $scope.getOrderDetilList = function(id) {
            $http({
                method: "GET",
                url: "api/orderDetil/order/"+id,
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(function (data) {
                $scope.orderDetilList = data.data;
            });
        };

        $scope.load = function(id) {
            Order.get({
                id: id
            }, function(result) {
                $scope.ro = result;
            });

            $scope.getOrderDetilList(id);
        };
        $scope.load($stateParams.id);

        $scope.delete = function(id) {
            OrderDetil.get({
                id: id
            }, function(result) {
                $scope.orderDetil = result;
                $('#deleteCategoryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function(id) {
            OrderDetil.delete({
                    id: id
                },
                function() {
                    $scope.load($stateParams.id);
                    $('#deleteCategoryConfirmation').modal('hide');
                    growl.info("Order Detil successfully deleted ", {});
                });
        };

        $scope.createFormDialog = function() {
            $('#createOrderDetil').modal('show');
        }
        $scope.editFormDialog = function(id) {
            OrderDetil.get({
                id: id
            }, function(result) {
                $scope.rodetil = result;
            });
            $('#editOrderDetil').modal('show');
        }
        $scope.create = function() {
            // console.log("masuk ")
            // $scope.submitted = true;

            // if ($scope.editForm.$valid) {
            //     console.log("sini ")
                $scope.rodetil.order = $scope.ro;
                OrderDetil.save($scope.rodetil, function() {
                    $scope.load($stateParams.id);
                    $('#createOrderDetil').modal('hide');
                    growl.info("Request Order Detil successfully saved ", {});
                });

            //}

        };

        $scope.saveOrderDetil = function() {
            // console.log("masuk ")
            // $scope.submitted = true;

            // if ($scope.editForm.$valid) {
            //     console.log("sini ")
                OrderDetil.update($scope.rodetil, function() {
                    $scope.load($stateParams.id);
                    $('#editOrderDetil').modal('hide');
                    growl.info("Request Order successfully edited ", {});
                });

            //}

        };


    });
