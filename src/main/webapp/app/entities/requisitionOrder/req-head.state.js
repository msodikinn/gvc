(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('req-head', {
                parent: 'entity',
                url: '/req-head',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/requisitionOrder/req-head-list.view.html',
                        controller: 'ReqHeadController'
                    }
                }
            }).state('req-head-create', {
                parent: 'entity',
                url: '/req-head/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/requisitionOrder/req-head-create.view.html',
                        controller: 'ReqHeadCreateController'
                    }
                }
            }).state('req-line-create', {
                parent: 'entity',
                url: '/req-line/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/requisitionOrder/req-line-create.view.html',
                        controller: 'ReqLineCreateController'
                    }
                }
            }).state('req-head-edit', {
                parent: 'entity',
                url: '/req-head/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/requisitionOrder/req-head-edit.view.html',
                        controller: 'ReqHeadEditController'
                    }
                }
            });

    }

})();
