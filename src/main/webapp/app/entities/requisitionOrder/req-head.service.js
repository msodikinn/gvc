'use strict';

angular.module('talarionApp')
    .factory('Order', function($resource) {
        return $resource('api/order/:id', {}, {
            'update': {
                method: 'PUT'
            }
        });
});
angular.module('talarionApp')
    .factory('OrderDetil', function($resource) {
        return $resource('api/orderDetil/:id', {}, {
            'update': {
                method: 'PUT'
            }
        });
});
