(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('organisasi', {
                parent: 'entity',
                url: '/organisasi',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/organisasi/organisasi-list.view.html',
                        controller: 'OrganisasiController'
                    }
                }
            }).state('organisasi-create', {
                parent: 'entity',
                url: '/organisasi/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/organisasi/organisasi-create.view.html',
                        controller: 'OrganisasiCreateController'
                    }
                }
            }).state('organisasi-edit', {
                parent: 'entity',
                url: '/organisasi/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/organisasi/organisasi-edit.view.html',
                        controller: 'organisasiEditController'
                    }
                }
            });

    }

})();
