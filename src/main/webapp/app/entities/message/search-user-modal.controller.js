angular.module('talarionApp').controller('SearchUserModalController', function($uibModalInstance, User, ParseLinks) {
    var vm = this;

    vm.callServerUser = function callServerUser(tableState) {

        vm.isLoading = true;

        var pagination = tableState.pagination;

        var start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10; // Number of entries showed per page.
        var numberOfPages = pagination.numberOfPages || 1;
        var page = start / number + 1;

        var firstNameSearchCrit = '';

        if (tableState.search.predicateObject != undefined) {
            firstNameSearchCrit = tableState.search.predicateObject.firstName || '';
        }

        User.queryWithSearch({
            page: page - 1,
            per_page: 10,
            firstName: firstNameSearchCrit
        }, function(result, headers) {
            // console.log(JSON.stringify(result));
            vm.links = ParseLinks.parse(headers('link'));
            vm.userList = result;
            tableState.pagination.numberOfPages = 1 * headers('X-Total-Pages');
            tableState.pagination.number = 1 * headers('X-Size');
            tableState.pagination.start = tableState.pagination.number * (page - 1);
            vm.isLoading = false;

            // console.log('tableState.pagination.start: ' + tableState.pagination.start);
            // console.log('tableState.pagination.numberOfPages: ' + tableState.pagination.numberOfPages);
            // console.log('tableState.pagination.number: ' + tableState.pagination.number);

            // $state.go('setting-produk');
        });

    };

    vm.cancel = function() {
        $uibModalInstance.dismiss('cancel');
    };
});
