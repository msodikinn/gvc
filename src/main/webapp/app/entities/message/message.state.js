(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('message', {
                parent: 'entity',
                url: '/message',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/message/message-list.view.html',
                        controller: 'MessageController'
                    }
                }
            }).state('message-create', {
                parent: 'entity',
                url: '/message/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/message/message-create.view.html',
                        controller: 'MessageCreateController'
                    }
                }
            }).state('message-edit', {
                parent: 'entity',
                url: '/message/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/message/message-edit.view.html',
                        controller: 'MessageEditController'
                    }
                }
            }).state('message-detail', {
                parent: 'entity',
                url: '/message-detail/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/message/message-detail.view.html',
                        controller: 'MessageDetailController'
                    }
                }
            });

    }

})();
