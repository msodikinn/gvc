'use strict';

angular.module('talarionApp')
    .controller('MessageDetailController', function($scope, $state, $stateParams, Message, User, ParseLinks, growl, $interval, $anchorScroll, $location, $timeout) {

        $scope.compose = {};

        $scope.load = function(from) {

            $scope.page = 1;

            User.get({
                login: from
            }, function(result) {
                $scope.from = result;
            });

            Message.query({
                page: $scope.page,
                size: 10,
                from: from
            }, function(result, headers) {

                $scope.messageList = result;

            });

        };

        $scope.send = function() {

            $scope.compose.to = $scope.from;

            Message.save($scope.compose, function() {

                $scope.compose.text = '';
                $scope.load($stateParams.id);

            });

        }

        // console.log('$stateParams.id: ' + $stateParams.id);
        //Put in interval, first trigger after 10 seconds
        var theInterval = $interval(function() {
            $scope.load($stateParams.id);
        }.bind(this), 10000);

        $scope.$on('$destroy', function() {
            $interval.cancel(theInterval)
        });

        $scope.load($stateParams.id);

        // window.scrollTo(0, document.body.scrollHeight);
        $timeout(function() {
            $location.hash('writeMessage');
            $anchorScroll();
        })

    });
