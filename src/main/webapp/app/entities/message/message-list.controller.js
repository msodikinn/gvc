(function() {
    'use strict';

    angular
        .module('talarionApp')
        .controller('MessageController', MessageController);

    MessageController.$inject = ['$scope', '$state', 'Message', 'ParseLinks', 'growl', 'User', '$uibModal'];

    function MessageController($scope, $state, Message, ParseLinks, growl, User, $uibModal) {

        var vm = this;

        $scope.messageUnreadList = [];
        $scope.messageReadList = [];
        $scope.page = 1;
        $scope.loadAll = function() {
            Message.query({
                page: $scope.page,
                per_page: 20
            }, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.messageUnreadList = result;
            });
        };
        $scope.loadPage = function(page) {
            $scope.page = page;
            $scope.loadAll();
        };
        // $scope.loadAll();

        $scope.showUpdate = function(id) {
            Message.get({
                id: id
            }, function(result) {
                $scope.message = result;
                $('#saveMessageModal').modal('show');
            });
        };

        $scope.save = function() {
            if ($scope.author.id != null) {
                Message.update($scope.message,
                    function() {
                        $scope.refresh();
                    });
            } else {
                Message.save($scope.message,
                    function() {
                        $scope.refresh();
                    });
            }
        };

        $scope.delete = function(id) {
            Message.get({
                id: id
            }, function(result) {
                $scope.message = result;
                $('#deleteMessageConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function(id) {
            Message.delete({
                    id: id
                },
                function() {
                    $scope.loadAll();
                    $('#deleteMessageConfirmation').modal('hide');
                    $scope.clear();
                    growl.info("Message successfully deleted ", {});
                });
        };

        $scope.refresh = function() {
            $scope.loadAll();
            $('#saveMessageModal').modal('hide');
            $scope.clear();
        };

        $scope.clear = function() {

        };

        $scope.callServerUnread = function callServerUnread(tableState) {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10; // Number of entries showed per page.
            var numberOfPages = pagination.numberOfPages || 1;
            var page = start / number + 1;

            var nameSearchCrit = '';

            if (tableState.search.predicateObject != undefined) {
                nameSearchCrit = tableState.search.predicateObject.name || '';
            }

            Message.groupByFrom({
                page: page,
                size: 10,
                name: nameSearchCrit
            }, function(result, headers) {
                // $scope.links = ParseLinks.parse(headers('link'));
                $scope.messageUnreadList = result;
                // tableState.pagination.numberOfPages = 1 * headers('X-Total-Pages');
                // tableState.pagination.number = 1 * headers('X-Size');
                // tableState.pagination.start = tableState.pagination.number * (page - 1);
                // $scope.isLoading = false;
                //
                // console.log('tableState.pagination.start: ' + tableState.pagination.start);
                // console.log('tableState.pagination.numberOfPages: ' + tableState.pagination.numberOfPages);
                // console.log('tableState.pagination.number: ' + tableState.pagination.number);

                // $state.go('message');
            });

        };

        $scope.callServerRead = function callServerRead(tableState) {

            $scope.isLoading = true;

            var pagination = tableState.pagination;

            var start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10; // Number of entries showed per page.
            var numberOfPages = pagination.numberOfPages || 1;
            var page = start / number + 1;

            var nameSearchCrit = '';

            if (tableState.search.predicateObject != undefined) {
                nameSearchCrit = tableState.search.predicateObject.name || '';
            }

            Message.groupByFrom({
                page: page,
                size: 10,
                name: nameSearchCrit,
                read: true
            }, function(result, headers) {
                // $scope.links = ParseLinks.parse(headers('link'));
                $scope.messageReadList = result;
                // tableState.pagination.numberOfPages = 1 * headers('X-Total-Pages');
                // tableState.pagination.number = 1 * headers('X-Size');
                // tableState.pagination.start = tableState.pagination.number * (page - 1);
                // $scope.isLoading = false;
                //
                // console.log('tableState.pagination.start: ' + tableState.pagination.start);
                // console.log('tableState.pagination.numberOfPages: ' + tableState.pagination.numberOfPages);
                // console.log('tableState.pagination.number: ' + tableState.pagination.number);

                // $state.go('message');
            });

        };

        $scope.open = function(size, parentSelector) {
            var parentElem = parentSelector ?
                angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'search-user-modal.view.html',
                controller: 'SearchUserModalController',
                controllerAs: 'vm',
                size: size,
                appendTo: parentElem
            });

            modalInstance.result.then(function(selectedItem) {
                vm.selected = selectedItem;
            }, function() {
                // console.log('Modal dismissed at: ' + new Date());
            });
        };

    }
})();
