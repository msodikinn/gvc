'use strict';

angular.module('talarionApp')
    .factory('Message', function($resource) {
        return $resource('api/message/:id', {
            id: '@id'
        }, {
            'update': {
                method: 'PUT'
            },
            'groupByFrom': {
                method: 'GET',
                isArray: true,
                url: 'api/message/groupByFrom'
            },
            'selfCountUnread': {
                method: 'GET',
                url: 'api/message/self/countUnread'
            }
        });
    });
