(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('supplier', {
                parent: 'entity',
                url: '/supplier',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/supplier/supplier-list.view.html',
                        controller: 'SupplierController'
                    }
                }
            }).state('supplier-create', {
                parent: 'entity',
                url: '/supplier/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/supplier/supplier-create.view.html',
                        controller: 'SupplierCreateController'
                    }
                }
            }).state('supplier-edit', {
                parent: 'entity',
                url: '/supplier/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/supplier/supplier-edit.view.html',
                        controller: 'supplierEditController'
                    }
                }
            });

    }

})();
