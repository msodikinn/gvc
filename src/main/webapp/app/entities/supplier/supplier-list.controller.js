'use strict';

angular.module('talarionApp')
    .controller('SupplierController', function($scope, $state, Category, ParseLinks, growl) {
        $scope.page = 1;

        $scope.records = [
           {
                "id" : "1",
                "name" : "PT. SUKANDA DJAYA",
                "alamat" : "Jl. Pasir Putih Raya Kav. 1 Ancol Timur - Jakarta",
                "email" : "kevin.lim90@yahoo.com",
                "cp" : "Bp. Aries",
                "phone" : "081223278431"
            },{
                "id" : "2",
                "name" : "PT. LOTTE SHOPPING INDONESIA",
                "alamat" : "Komplek Greenville Blok AY No. 11A Jakarta",
                "email" : "doni@getra-agritek.com",
                "cp" : "Ibu Nengah Eva Luciana",
                "phone" : "085214968264"
            },{
                "id" : "3",
                "name" : "  PT. CIMORY DAIRY SHOP",
                "alamat" : "Rukan Taman Meruya N1-4 Jakarta 11620",
                "email" : "rosie@kelolaniaga.com",
                "cp" : "Bp. R.E. Hermawan",
                "phone" : "087877600507"
            },{
               "id" : "4",
                "name" : "VIRTUS VENTURAMA",
                "alamat" : "Komplek Agung Sedayu Blok J5-5 Harco Mangga Dua",
                "email" : "",
                "cp" : "Bp. Andhika",
                "phone" : "081908977495"
            }
        ]
       

    });
