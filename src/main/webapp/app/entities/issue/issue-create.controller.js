'use strict';

/**
 * @ngdoc function
 * @name yoAngularApp.controller:AboutCtrl
 * @description # AboutCtrl Controller of the yoAngularApp
 */
angular.module('talarionApp').controller('IssueCreateController',
    function($scope, $state, $stateParams, Issue, growl , Order, ParseLinks) {

      $scope.submitted = false;

        $scope.ro = {};
        $scope.ro.order = {};


        $scope.dateOptions = {
            formatYear: 'yyyy',
            //minDate: new Date(),
            startingDay: 1
        };

        $scope.popupStartDate = {
            opened: false
        };

        $scope.format = 'yyyy-MM-dd';
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.showLookup = function() {
            $('#lookupDialog').modal('show');
        };

        $scope.selectLookup = function(category) {

            $scope.ro.order = category;
            //$scope.ro.customer = $scope.ro.order.customer

            $('#lookupDialog').modal('hide');
        };

        $scope.openStartDate = function() {
            $scope.popupStartDate.opened = true;
        };
        $scope.create = function() {

            $scope.submitted = true;

            if ($scope.createForm.$valid) {

                $scope.ro.issueStatus = '0';
                console.log($scope.ro)
                Issue.save($scope.ro, function() {

                    growl.info("Issue Order successfully added ", {});

                    $state.go('issue')

                });

            }

        };

        $scope.callServerOrder = function callServerOrder(tableState) {

            $scope.isLoading = true;
            $scope.orderTableState = tableState;
            var pagination = tableState.pagination;

            var start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10; // Number of entries showed per page.
            var numberOfPages = pagination.numberOfPages || 1;
            var page = start / number + 1;

            var orderNoSearchCrit = '';

            if(tableState.search.predicateObject != undefined){
              orderNoSearchCrit = tableState.search.predicateObject.orderNo || '';
            }

            Order.query({
                page: page,
                size: 10,
                ispurchase: "N",
                orderNo: orderNoSearchCrit
            }, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.orderList = result;
                angular.forEach($scope.orderList, function(data){
                    if(data.orderStatus == '1')
                        data.orderStatus = 'Proses'
                    else
                        data.orderStatus = 'Draft'
                });
                tableState.pagination.numberOfPages = 1 * headers('X-Total-Pages');
                tableState.pagination.number = 1 * headers('X-Size');
                tableState.pagination.start = tableState.pagination.number * (page - 1);
                $scope.isLoading = false;

                console.log('tableState.pagination.start: ' + tableState.pagination.start);
                console.log('tableState.pagination.numberOfPages: ' + tableState.pagination.numberOfPages);
                console.log('tableState.pagination.number: ' + tableState.pagination.number);

            });

        };

    });
