(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('issue', {
                parent: 'entity',
                url: '/issue',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/issue/issue-list.view.html',
                        controller: 'IssueController'
                    }
                }
            }).state('issue-create', {
                parent: 'entity',
                url: '/issue/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/issue/issue-create.view.html',
                        controller: 'IssueCreateController'
                    }
                }
            }).state('issue-edit', {
                parent: 'entity',
                url: '/issue/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/issue/issue-edit.view.html',
                        controller: 'ReqHeadEditController'
                    }
                }
            });

    }

})();
