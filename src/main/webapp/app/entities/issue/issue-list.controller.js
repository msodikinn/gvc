'use strict';

angular.module('talarionApp')
    .controller('IssueController', function($scope, $state, Issue, ParseLinks, growl) {
        $scope.page = 1;
        $scope.orderTableState = [];

        $scope.delete = function(id) {
            Issue.get({
                id: id
            }, function(result) {
                $scope.category = result;
                $('#deleteCategoryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function(id) {
            Issue.delete({
                    id: id
                },
                function() {
                    $scope.callServer($scope.orderTableState);
                    $('#deleteCategoryConfirmation').modal('hide');
                    growl.info("Issue successfully deleted ", {});
                });
        };

        $scope.callServer = function callServer(tableState) {

            $scope.isLoading = true;
            $scope.orderTableState = tableState;
            var pagination = tableState.pagination;

            var start = pagination.start || 0; // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10; // Number of entries showed per page.
            var numberOfPages = pagination.numberOfPages || 1;
            var page = start / number + 1;

            var orderNoSearchCrit = '';

            if(tableState.search.predicateObject != undefined){
              orderNoSearchCrit = tableState.search.predicateObject.orderNo || '';
            }

            Issue.query({
                page: page,
                size: 10,
            }, function(result, headers) {
                $scope.links = ParseLinks.parse(headers('link'));
                $scope.issueList = result;
                angular.forEach($scope.issueList, function(data){
                    if(data.issueStatus == '1')
                        data.issueStatus = 'Proses'
                    else
                        data.issueStatus = 'Draft'
                });
                tableState.pagination.numberOfPages = 1 * headers('X-Total-Pages');
                tableState.pagination.number = 1 * headers('X-Size');
                tableState.pagination.start = tableState.pagination.number * (page - 1);
                $scope.isLoading = false;

                console.log('tableState.pagination.start: ' + tableState.pagination.start);
                console.log('tableState.pagination.numberOfPages: ' + tableState.pagination.numberOfPages);
                console.log('tableState.pagination.number: ' + tableState.pagination.number);

                $state.go('issue');
            });

        };

       

    });
