'use strict';

angular.module('talarionApp')
    .factory('Issue', function($resource) {
        return $resource('api/issueorder/:id', {}, {
            'update': {
                method: 'PUT'
            }
        });
});
