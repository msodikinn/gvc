(function() {
    'use strict';

    angular
        .module('talarionApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('purchase', {
                parent: 'entity',
                url: '/purchase',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/purchaseOrder/purchase-head-list.view.html',
                        controller: 'PurchaseController'
                    }
                }
            }).state('purchase-create', {
                parent: 'entity',
                url: '/purchase/create',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/purchaseOrder/purchase-head-create.view.html',
                        controller: 'PurchaseCreateController'
                    }
                }
            }).state('purchase-edit', {
                parent: 'entity',
                url: '/purchase/:id',
                data: {
                    authorities: ['ROLE_USER']
                },
                views: {
                    'content@': {
                        templateUrl: 'app/entities/purchaseOrder/purchase-head-edit.view.html',
                        controller: 'PurchaseEditController'
                    }
                }
            });

    }

})();
