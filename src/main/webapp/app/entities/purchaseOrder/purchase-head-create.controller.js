'use strict';

/**
 * @ngdoc function
 * @name yoAngularApp.controller:AboutCtrl
 * @description # AboutCtrl Controller of the yoAngularApp
 */
angular.module('talarionApp').controller('PurchaseCreateController',
    function($scope, $state, $stateParams, Category, growl, Order) {

      $scope.submitted = false;

        $scope.ro = {};

        $scope.dateOptions = {
            formatYear: 'yyyy',
            //minDate: new Date(),
            startingDay: 1
        };

        $scope.popupStartDate = {
            opened: false
        };

        $scope.format = 'yyyy-MM-dd';
        $scope.altInputFormats = ['M!/d!/yyyy'];

        $scope.openStartDate = function() {
            $scope.popupStartDate.opened = true;
        };
        $scope.create = function() {

            $scope.submitted = true;

            if ($scope.createForm.$valid) {

                $scope.ro.orderStatus = '0';
                $scope.ro.ispurchase = 'Y';
                Order.save($scope.ro, function(order) {

                    growl.info("Purchase Order <" + order.id + "> successfully added ", {});

                   // $state.go('req-head')
                    $state.go('purchase-edit', {
                        'id': order.id
                    });
                });

            }

        };

    });
